package self_limit

import "net/http"

type SelfLimitingClient interface {
	Run(*Request) *Response

	Store(Store) SelfLimitingClient
	HTTP(*http.Client) SelfLimitingClient
}
