package self_limit

import (
	"errors"
	"fmt"
	"github.com/epic-glue/self-control"
	"github.com/uber-go/zap"
	"io/ioutil"
	"net/http"
)

type Client struct {
	rules      *Rules
	usingRule  *Rule
	store      Store
	httpClient *http.Client
}

func NewClient() *Client {
	return NewClientWithRules(NewDefaultRules())
}

func NewClientWithRules(rules *Rules) *Client {
	log.SetLevel(zap.ErrorLevel)

	log.Info(fmt.Sprintf("New client with %d rules", len(rules.rules)))

	return &Client{
		rules: rules,
		store: NewMemoryStore(),
		httpClient: &http.Client{
			Transport: &self_control.Transport{
				EnableLogging: true,
			},
		},
	}
}

// Replace Store module
func (c *Client) Store(store Store) *Client {
	c.store = store
	return c
}

// Replace HTTP Client
func (c *Client) HTTP(httpClient *http.Client) *Client {
	c.httpClient = httpClient
	return c
}

// Requests
func (c *Client) Run(request *Request) *Response {
	if err := c.pre(request); err != nil {
		return NewErrorResponse(err)
	}

	httpRequest, err := http.NewRequest(request.Method, request.URL.String(), nil)
	httpRequest.Header = request.Header
	httpRequest.Header.Add("Host", request.URL.Host)
	if err != nil {
		log.Error(err.Error())
		return NewErrorResponse(err)
	}

	log.Info(request.Method, zap.String("url", request.URL.String()))

	httpResponse, err := c.httpClient.Do(httpRequest)
	if err != nil {
		log.Error(err.Error())
		return NewErrorResponse(err)
	}

	if httpResponse.StatusCode != 200 {
		log.Error(httpResponse.Status)
		return NewErrorResponse(errors.New(httpResponse.Status))
	}

	defer httpResponse.Body.Close()
	body, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {
		log.Error(err.Error())
		return NewErrorResponse(err)
	}

	log.Info("Response", zap.Int("code", httpResponse.StatusCode), zap.Int("size", len(body)))
	log.Debug("Response", zap.String("json", string(body)))

	response := Response{
		Status:     httpResponse.Status,
		StatusCode: httpResponse.StatusCode,
		Proto:      httpResponse.Proto,
		ProtoMajor: httpResponse.ProtoMajor,
		ProtoMinor: httpResponse.ProtoMinor,
		Request:    request,
		Data:       body,
		TokenLimit: NewTokenLimitFromHeader(httpResponse.Header),
	}

	c.post(&response)

	return &response
}
