package self_limit

import (
	"github.com/spf13/cast"
	"net/http"
	"time"
)

type TokenLimit struct {
	Remaining int
	ResetAt   *time.Time
}

func NewTokenLimitFromHeader(h http.Header) *TokenLimit {
	return parseXRateLimits(h)
}

func parseXRateLimits(h http.Header) *TokenLimit {
	var (
		remaining int
		resetAt   *time.Time
	)

	if h.Get("X-Rate-Limit-Remaining") != "" {
		remaining = cast.ToInt(h.Get("X-Rate-Limit-Remaining"))
	} else {
		return nil
	}

	if h.Get("X-Rate-Limit-Reset") != "" {
		currentEpochNano := cast.ToString(time.Now().UnixNano())
		currentEpoch := cast.ToString(time.Now().Unix())
		resetTimestamp := cast.ToInt64(h.Get("X-Rate-Limit-Reset"))
		v := time.Time{}

		// Try EPOCH
		if len(currentEpochNano) <= len(h.Get("X-Rate-Limit-Reset")) {
			// Assume it's EPOCH with milliseconds and convert it to time
			v = time.Unix(resetTimestamp/1000, 0)
		} else if len(currentEpoch) <= len(h.Get("X-Rate-Limit-Reset")) {
			// Assume it's EPOCH and convert it to time
			v = time.Unix(resetTimestamp, 0)
		} else {
			// Use current time + number of seconds (hopefully) left provided by API
			v = time.Unix(cast.ToInt64(currentEpoch)+resetTimestamp, 0)
		}

		resetAt = &v
	}

	return &TokenLimit{
		Remaining: remaining,
		ResetAt:   resetAt,
	}
}
