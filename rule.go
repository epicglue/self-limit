package self_limit

import (
	"regexp"
	"time"
)

type Rule struct {
	RequestType   *string
	PathMatcher   *regexp.Regexp
	Default       *int
	RefreshRate   *time.Duration
	CurrentValue  int
	OriginalValue *int
	ResetAt       *time.Time
}

func NewMatchAllRule() *Rule {
	return &Rule{
		PathMatcher: regexp.MustCompile(".*"),
	}
}

func (r Rule) doesUrlMatch(reqType string, url string) bool {
	return (r.RequestType == nil || *r.RequestType == reqType) && r.PathMatcher.MatchString(url)
}
