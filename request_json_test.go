package self_limit_test

import (
	"github.com/epic-glue/self-limit"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewJSONRequest(t *testing.T) {
	req := self_limit.NewJSONRequest("http://localhost")

	assert.Equal(t, "http://localhost", req.URL.String())
	assert.Equal(t, "application/json", req.Header.Get("Accept"))
	assert.Equal(t, "application/json", req.Header.Get("Content-Type"))
	assert.Empty(t, req.Data)
}
