package self_limit_test

import (
	"github.com/epic-glue/self-limit"
	"github.com/spf13/cast"
	"github.com/stretchr/testify/assert"
	"io"
	"net/http"
	"net/http/httptest"
	"net/url"
	"testing"
	"time"
)

var fakeResponse string = `[1,2,3]`

func fakeHttpClient(code int, header map[string]string, body string) *http.Client {
	server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		for k, v := range header {
			w.Header().Set(k, v)
		}
		w.WriteHeader(code)
		io.WriteString(w, body)
	}))

	transport := &http.Transport{
		Proxy: func(req *http.Request) (*url.URL, error) {
			return url.Parse(server.URL)
		},
	}

	return &http.Client{
		Transport: transport,
	}
}

func TestClientWithDefaultToken(t *testing.T) {
	req := self_limit.NewGetRequest("http://hacker-news.firebaseio.com/v0/topstories.json")

	defaultValue := 2
	defaultRefreshRate := 1 * time.Hour

	rule := self_limit.NewMatchAllRule()
	rule.Default = &defaultValue
	rule.RefreshRate = &defaultRefreshRate

	c := self_limit.NewClientWithRules(self_limit.NewRules([]*self_limit.Rule{
		rule,
	})).HTTP(fakeHttpClient(
		http.StatusOK,
		map[string]string{},
		fakeResponse,
	))

	resp := c.Run(req)
	assert.Equal(t, resp.Data, []byte(fakeResponse))
	assert.Equal(t, resp.TokenLimit.Remaining, 1)
	assert.True(t, resp.IsOK())

	resp = c.Run(req)
	assert.Equal(t, resp.Data, []byte(fakeResponse))
	assert.Equal(t, resp.TokenLimit.Remaining, 0)
	assert.True(t, resp.IsOK())

	resp = c.Run(req)
	assert.Nil(t, resp.TokenLimit)
	assert.Nil(t, resp.Data)
	assert.False(t, resp.IsOK())
}

func TestClientWithFirstXToken(t *testing.T) {
	req := self_limit.NewGetRequest("http://hacker-news.firebaseio.com/v0/topstories.json")

	resetAt := time.Now().Add(5 * time.Minute)

	c := self_limit.NewClient()

	c.HTTP(fakeHttpClient(
		http.StatusOK,
		map[string]string{
			"X-Rate-Limit-Remaining": "1",
			"X-Rate-Limit-Reset":     cast.ToString(resetAt.Unix()),
		},
		fakeResponse),
	)

	resp := c.Run(req)
	assert.Equal(t, resp.Data, []byte(fakeResponse))
	assert.Equal(t, resp.TokenLimit.Remaining, 1)
	assert.Equal(t, resp.TokenLimit.ResetAt.Unix(), resetAt.Unix())
	assert.True(t, resp.IsOK())

	c.HTTP(fakeHttpClient(
		http.StatusOK,
		map[string]string{
			"X-Rate-Limit-Remaining": "0",
			"X-Rate-Limit-Reset":     cast.ToString(resetAt.Unix()),
		},
		fakeResponse),
	)

	resp = c.Run(req)
	assert.Equal(t, resp.Data, []byte(fakeResponse))
	assert.Equal(t, resp.TokenLimit.Remaining, 0)
	assert.True(t, resp.IsOK())

	c.HTTP(fakeHttpClient(
		http.StatusTooManyRequests,
		map[string]string{},
		fakeResponse),
	)

	resp = c.Run(req)
	assert.Nil(t, resp.TokenLimit)
	assert.Nil(t, resp.Data)
	assert.NotNil(t, resp.Error)
	assert.Equal(t, self_limit.ERR_NO_TOKENS_LEFT, resp.Error.Error())
	assert.False(t, resp.IsOK())
}

// TODO: tests with multiple rules and endpoints
