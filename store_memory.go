package self_limit

import (
	cache "github.com/patrickmn/go-cache"
	"time"
)

type MemoryStore struct {
	Cache *cache.Cache
}

func NewMemoryStore() Store {
	return MemoryStore{
		Cache: cache.New(cache.NoExpiration, DefaultCleanUpInterval),
	}
}

func (s MemoryStore) Get(key string) *TokenLimit {
	item, ok := s.Cache.Items()[key]

	if !ok {
		return nil
	}

	return item.Object.(*TokenLimit)
}

func (s MemoryStore) Set(key string, limit *TokenLimit) {
	var expiry time.Duration

	if limit == nil {
		return
	}

	if limit.ResetAt != nil && time.Now().Before(*limit.ResetAt) {
		resetAt := *limit.ResetAt
		expiry = resetAt.Sub(time.Now())
	}

	s.Cache.Set(key, limit, expiry)
}
