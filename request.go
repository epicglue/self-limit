package self_limit

import (
	"net/http"
	"net/url"
)

type Request struct {
	token  Token
	Header http.Header
	Method string
	URL    *url.URL
	Data   []byte
}

func NewRequest(method string, address string) *Request {
	parsedUrl, err := url.Parse(address)
	if err != nil {
		return nil
	}

	return &Request{
		Header: make(http.Header),
		Method: method,
		URL:    parsedUrl,
	}
}

func NewGetRequest(address string) *Request {
	return NewRequest(http.MethodGet, address)
}

func (r *Request) addHeader(key string, value string) {
	if r.Header == nil {
		r.Header = make(http.Header)
	}

	r.Header.Add(key, value)
}

func (r *Request) Token(token Token) *Request {
	r.token = token
	r.addHeader(token.Header())

	return r
}

func (r *Request) Payload(payload []byte) *Request {
	r.Data = payload

	return r
}

func (r *Request) UserAgent(userAgent string) *Request {
	r.addHeader("User-Agent", userAgent)

	return r
}
