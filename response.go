package self_limit

type Response struct {
	Status     string // e.g. "200 OK"
	StatusCode int    // e.g. 200
	Proto      string // e.g. "HTTP/1.0"
	ProtoMajor int    // e.g. 1
	ProtoMinor int    // e.g. 0

	Request    *Request
	TokenLimit *TokenLimit
	Data       []byte
	Error      error
}

func NewErrorResponse(err error) *Response {
	return &Response{
		Error: err,
	}
}

func (r Response) IsOK() bool {
	return r.Error == nil
}
