package self_limit

import "github.com/uber-go/zap"

var log = zap.New(zap.NewJSONEncoder(zap.NoTime()))
