package self_limit

type Token interface {
	Value() string
	Header() (string, string)
}

type BearerToken struct {
	token string
}

func NewBearerToken(token string) Token {
	return BearerToken{
		token: token,
	}
}

func (t BearerToken) Value() string {
	return t.token
}

func (t BearerToken) Header() (string, string) {
	return "Authorization", "Bearer " + t.token
}
