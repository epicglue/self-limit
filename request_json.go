package self_limit

func NewJSONRequest(address string) *Request {
	req := NewGetRequest(address)
	req.addHeader("Accept", "application/json")
	req.addHeader("Content-Type", "application/json")

	return req
}
