package self_limit

import "time"

const (
	DefaultCleanUpInterval = 4 * time.Hour
)

type Store interface {
	Get(key string) *TokenLimit
	Set(key string, value *TokenLimit)
}
