package self_limit

import (
	"errors"
	"fmt"
	"github.com/uber-go/zap"
	"time"
)

const (
	ERR_NO_TOKENS_LEFT = "No tokens left"
)

func (c *Client) pre(request *Request) error {
	c.usingRule = c.findMatchingRule(request)

	if !c.checkLimits(request) {
		return errors.New(ERR_NO_TOKENS_LEFT)
	}

	return nil
}

func (c *Client) post(response *Response) error {
	c.updateLimits(response)

	return nil
}

// Find matching rule
func (c *Client) findMatchingRule(request *Request) *Rule {
	rule := c.rules.match(request.Method, request.URL.Path)

	//if rule == nil {
	//	log.Info("Couldn't find matching rule")
	//} else {
	//	log.Info("Found rule", zap.String("rule", rule.PathMatcher.String()))
	//}

	return rule
}

// Get limits
func (c *Client) checkLimits(request *Request) bool {
	log.Info("Cache.GET", zap.String("key", c.buildKeyForStore(request)))

	tokenLimit := c.store.Get(c.buildKeyForStore(request))

	if tokenLimit == nil {
		return true
	}

	if tokenLimit.Remaining > 0 {
		log.Info(fmt.Sprintf("Has %d tokens left", tokenLimit.Remaining))
		return true
	}

	if tokenLimit.ResetAt != nil && tokenLimit.ResetAt.Unix() > 0 && tokenLimit.ResetAt.Before(time.Now()) {
		log.Info("Previous token expired", zap.Time("now", time.Now()), zap.Time("token_reset_at", *tokenLimit.ResetAt))
		return true
	}

	log.Info("No tokens left")
	return false
}

// Store new limits
func (c *Client) updateLimits(response *Response) {
	if response.TokenLimit == nil {
		if currentLimits := c.store.Get(c.buildKeyForStore(response.Request)); currentLimits == nil {
			response.TokenLimit = c.tokenLimitByDefaultValues()

			if response.TokenLimit != nil {
				log.Info("New TokenLimit using default values", zap.Int("remaining", response.TokenLimit.Remaining))
				return
			}
		} else {
			currentLimits.Remaining = currentLimits.Remaining - 1
			response.TokenLimit = currentLimits

			log.Info("Update TokenLimit previously created from default values", zap.Int("remaining", response.TokenLimit.Remaining))
		}
	} else {
		log.Info("Successfuly parsed headers", zap.Int("left", response.TokenLimit.Remaining))
	}

	log.Info("Cache.SET", zap.String("key", c.buildKeyForStore(response.Request)))

	c.store.Set(c.buildKeyForStore(response.Request), response.TokenLimit)
}

// Helper
func (c *Client) buildKeyForStore(request *Request) string {
	// TODO: need prefix too?

	token := ""
	if request.token != nil {
		token = request.token.Value()
	}

	return fmt.Sprintf("%s:%s:%s:%s", request.URL.Host, token, request.Method, c.usingRule.PathMatcher.String())
}

func (c *Client) tokenLimitByDefaultValues() *TokenLimit {
	var tokenLimit *TokenLimit

	if c.usingRule.Default != nil {
		tokenLimit = &TokenLimit{
			Remaining: *c.usingRule.Default - 1,
		}

		if c.usingRule.RefreshRate != nil {
			resetAt := time.Now().Add(*c.usingRule.RefreshRate)
			tokenLimit.ResetAt = &resetAt
		}

		log.Info("New TokenLimit", zap.Int("remaining", tokenLimit.Remaining), zap.Time("reset_at", *tokenLimit.ResetAt))
	} else {
		log.Info("Cannot create token limit without default values")
	}

	return tokenLimit
}
