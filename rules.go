package self_limit

type Rules struct {
	rules []*Rule
}

func NewRules(rules []*Rule) *Rules {
	return &Rules{
		rules: rules,
	}
}

func NewDefaultRules() *Rules {
	return NewRules([]*Rule{
		NewMatchAllRule(),
	})
}

func (r Rules) match(reqType string, endpoint string) *Rule {
	for _, rule := range r.rules {
		if rule.doesUrlMatch(reqType, endpoint) {
			return rule
		}
	}

	return nil
}
