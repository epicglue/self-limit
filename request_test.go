package self_limit_test

import (
	"github.com/epic-glue/self-limit"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewRequest(t *testing.T) {
	req := self_limit.NewGetRequest("http://localhost")

	assert.Equal(t, "http://localhost", req.URL.String())
	assert.Empty(t, req.Header)
	assert.Empty(t, req.Data)
}

func TestRequest_Token(t *testing.T) {
	req := self_limit.NewGetRequest("http://localhost").Token(self_limit.NewBearerToken("token"))

	assert.Equal(t, "Bearer token", req.Header.Get("Authorization"))
	assert.Empty(t, req.Data)
}

func TestRequest_Payload(t *testing.T) {
	req := self_limit.NewGetRequest("http://localhost").Payload([]byte("payload"))

	assert.Empty(t, req.Header)
	assert.Equal(t, []byte("payload"), req.Data)
}

func TestRequest_UserAgent(t *testing.T) {
	req := self_limit.NewGetRequest("http://localhost").UserAgent("test agent")

	assert.Equal(t, "test agent", req.Header.Get("User-Agent"))
	assert.Empty(t, req.Data)
}
